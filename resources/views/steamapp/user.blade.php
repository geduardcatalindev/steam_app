<html>
<head>
	<meta charset="UTF-8">
	<title>User</title>
	<script type="text/javascript" src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
</head>
<body>

	@php
		// print_r($userDetails);
		$user = json_decode($userDetails);
		print_r($user);
		// echo $user->response->players[0]->steamid;
	@endphp

	</br>
	</br>

	<button class="redirectToOwnedGames">Owned Games</button>

	<script>
		$(".redirectToOwnedGames").on("click", function(){
			var currentLocation = window.location.href;
			
			if(currentLocation[currentLocation.length - 1] === "?")
			{
				var redirect = window.location.href.slice(0, -1);
				window.location.href = redirect + "/ownedgames";
			}
			else
			{
				window.location.href += "/ownedgames";
			}
		});
	</script>

</body>
</html>
