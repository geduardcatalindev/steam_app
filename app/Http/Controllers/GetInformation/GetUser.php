<?php

namespace App\Http\Controllers\GetInformation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetUser extends Controller
{
    public function getUser($username)
    {
        try
        {
            $steamCommunity = file_get_contents("http://steamcommunity.com/id/" . $username . "/?xml=1");
            $xmlToObject = simplexml_load_string($steamCommunity);
            $steamID64 = $xmlToObject->steamID64;
            $apiKey = "781ECC3E80C8FCAE4A7BD9A32D48E68F";

            $url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" . $apiKey . "&steamids=" . $steamID64;

            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            curl_close($ch);

            return $data;
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function returnToView($username)
    {
        $userDetails = GetUser::getUser($username);
        
        return view('steamapp.user', compact('userDetails'));
    }
}
