<?php

namespace App\Http\Controllers\GetInformation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetOwnedGames extends Controller
{
    public function getOwnedGames($username)
    {
        try
        {
            $steamCommunity = file_get_contents("http://steamcommunity.com/id/" . $username . "/?xml=1");
            $xmlToObject = simplexml_load_string($steamCommunity);
            $steamID64 = $xmlToObject->steamID64;
            $apiKey = "781ECC3E80C8FCAE4A7BD9A32D48E68F";

            $url = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=" . $apiKey . "&steamid=" . $steamID64 . "&format=json";
            
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            curl_close($ch);

            return $data;
        }
        catch(\Exception $ex)
        {
            return $ex;
        }
    }

    public function returnToView($username)
    {
        $userOwnedGames = GetOwnedGames::getOwnedGames($username);

        return view('steamapp.ownedGames', compact('userOwnedGames'));
    }
}
